const data = `20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT`

var numberOfLineBreaks = (data.match(/\n/g) || []).length;
// console.log('Number of breaks: ' + numberOfLineBreaks);

const entry = data.split('\n');

// console.log(entry[5]);

let alert = {};
const allMessages = [];
const entryMap = {};
entry.forEach(e => {
    // console.log(e);
    let line = e.split('|');
    // console.log(line[0]);
    reading = {
        "satelliteId": line[1],
        "severity": getSeverity(line),
        "component": line[7],
        "timestamp": line[0]
    }
    allMessages.push(reading)
});

const batteryMessages = allMessages.filter( (message, index, list) => {
    // console.log(convertStringToDate(message.timestamp), convertStringToDate(list[0].timestamp));
    const baseTime = convertStringToDate(list[0].timestamp);
    const after5mins = new Date(baseTime.getTime() + 5*60000);
    const time = convertStringToDate(message.timestamp);
    return message.component === 'BATT' && time <= after5mins && message.severity === 'RED LOW'
});

const tStatMessages = allMessages.filter( (message, index, list) => {
    // console.log(convertStringToDate(message.timestamp), convertStringToDate(list[0].timestamp));
    const baseTime = convertStringToDate(list[0].timestamp);
    const after5mins = new Date(baseTime.getTime() + 5*60000);
    const time = convertStringToDate(message.timestamp);
    // console.log(message.component === 'TSTAT' && time <= after5mins && message.severity === 'RED HIGH');
    return message.component === 'TSTAT' && time <= after5mins && message.severity === 'RED HIGH'
});
//  console.log(allMessages);


let batteryMessagesCount = {}
batteryMessages.forEach( (alert, index) => {
    const key = alert.satelliteId;
    let count = (batteryMessagesCount[key] && batteryMessagesCount[key].count || 0) + 1;
    batteryMessagesCount[key] = {count, index};
});

let tStatMessagesCount = {}
tStatMessages.forEach( (alert, index) => {
    const key = alert.satelliteId;
    let count = (tStatMessagesCount[key] && tStatMessagesCount[key].count || 0) + 1;
    tStatMessagesCount[key] = {count, index};
});

const alerts =[];
for( let key in batteryMessagesCount ) {
    const b = batteryMessagesCount[key];
    if (b.count >= 3) {
        alerts.push(batteryMessages[b.index])
    }
};

// console.log(alerts);

for( let key in tStatMessagesCount ) {
    const t = tStatMessagesCount[key];
    if (t.count >= 3) {
        alerts.push(tStatMessages[t.index])
    }
};

console.log(alerts);

function getSeverity(reading) {
    let component = reading[7];
    let raw = parseFloat(reading[6]);
    let redLowLimit = parseFloat(reading[5]);
    let yellowLowLimit = parseFloat(reading[4]);
    let yellowHighLimit = parseFloat(reading[3]);
    let redHighLimit = parseFloat(reading[2]);
    if (raw <= redLowLimit) {
        return "RED LOW";
    } else if (raw <= yellowLowLimit) {
        return "YELLOW LOW";
    } else if (raw <= yellowHighLimit){
        return "YELLOW HIGH";
    } else {
        return "RED HIGH"
    }
}

function convertStringToDate(dateString) {
    let ds = dateString.split(' ');
    let date = ds[0];
    let time = ds[1].split(':');
    let sec = time[2].split('.');

    return new Date(date.substring(0,3), date.substring(4,5), date.substring(6,7), time[0], time[1], sec[0], sec[1]);
}